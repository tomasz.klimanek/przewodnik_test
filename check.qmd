---
title: "check"
---

```{r}
library(knitr)
dt1 <- data.frame("Obszar" = c("A"), 
                  "Pracujący w A" = c(87),
                  "Mieszkańcy" = c(232),
                  "Pracujący i mieszkający" = c(69),
                  check.names = FALSE)
```

```{r}
#| label: tbl-cars
#| tbl-cap: "Cars"
#| tbl-colwidths: [60,40]

kable(head(dt1))
```