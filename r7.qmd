---
fontcolor: darkblue
backgroundcolor: antiquewhite
engine: knitr
bibliography: bibliografia.bib
csl: apa.csl
reference-section-title: Bibliografia
mainfont: Fira Sans
fontsize: 12pt
---

```{css, echo = FALSE}
.justify {
  text-align: justify !important
}
```

# Rekomendowane metody oceny ryzyka ujawnienia danych objętych tajemnicą statystyczną

## Dane zagregowane

::: justify

Tablice statystyczne (a także ilustracje graficzne) zawierają agregaty danych obliczane na podstawie danych z&nbsp;badania reprezentacyjnego lub pełnego albo konglomeratu badania reprezentacyjnego i pełnego. Ponieważ w naliczaniu agregatów dla badań reprezentacyjnych istotną rolę odgrywają wagi dla poszczególnych rekordów wynikające z przyjętego schematu losowania z ewentualną kalibracją (dla obserwacji z badania pełnego przyjmujemy wagę równą 1), zatem w ocenie ryzyka ujawnienia należy wziąć pod uwagę następujące wymogi wynikające z Ustawy i ugruntowanej pragmatyki w zakresie kontroli ujawniania danych:

* liczba jednostek wchodzących w skład agregatu w populacji bądź jej oszacowanie musi wynosić co najmniej 3 (wymaganie dotyczące komórek, jeśli chodzi o liczbę szacowanej lub znanej liczby jednostek w&nbsp;populacji),
* udział najwyższej spośród wartości jednostkowych cechy z próby wchodzących w skład agregatu w&nbsp;wartości oszacowania wielkości agregatu w populacji jest nie większy niż 75% (wymaganie dotyczące udziału wartości jednostkowych w wielkości sumarycznej).

Agregat, który nie spełnia przynajmniej jednego z tych warunków uznawany jest za stwarzający zagrożenie identyfikacją konkretnej jednostki (czyli krótko mówiąc – ryzykowny). Powyższe warunki – wedle zapisów Ustawy – stosuje się przede wszystkim do danych charakteryzujących wyniki ekonomiczno-finansowe podmiotów gospodarki narodowej prowadzących działalność gospodarczą. Jednak w razie potrzeby można oprzeć się na nich także w przypadku danych z badań społecznych oraz spisów powszechnych, o ile określone dane uważa się za wrażliwe i o ile nie zastosowano już skuteczniejszych metod ochrony tajemnicy statystycznej.

Istnieje też wymaganie dotyczące próby. Jako ryzykowną należy mianowicie uznać sytuację, gdy w próbie występuje tylko jedna jednostka reprezentująca agregat lub występują dwie jednostki, których wagi uogólniające są identyczne lub różnią się o mniej niż 30% wielkości mniejszej z nich; warunek ten może być pominięty przy ocenie konieczności ukrywania przekrojów objętych tajemnicą statystyczną (możliwość nieukrywania przekrojów mimo wystąpienia powyższej sytuacji) jeżeli z prawdopodobieństwem graniczącym z&nbsp;pewnością można przyjąć, że liczba jednostek wchodzących w skład agregatu w populacji generalnej wynosi co najmniej 3.

Ocena ryzyka ujawnienia informacji wrażliwych zależy w największym stopniu od liczby komórek wrażliwych w&nbsp;tablicy, czyli takich, które nie spełniają przynajmniej jednego z powyższych warunków. Im większa liczba takich komórek, tym ryzyko wyższe. Ogólnie rzecz biorąc, prawdopodobieństwo wtórnej identyfikacji jednostki $i$ przynależnej do komórki $C_l$ wynosi $1/F_l$ (gdzie $F_l$ to liczba jednostek w populacji przynależących do tej komórki). Warto też zauważyć, że wymóg maksymalnego udziału nie większego niż 75% to w istocie zasada *(n,k)* dominacji przy $n=1$ i $k=75$. **Można tu zatem zastosować ocenę ryzyka zaproponowaną przez Domingo–Ferrera i Torrę [-@domingo2004disclosure], opartą na poziomie odchylenia sumy** ***n*** **największych udziałów w komórce od** ***k%***. Chodzi o umożliwienie eliminacji ryzyka tego, że w komórkach, w których różnice między udziałami są znaczne, możliwa precyzja oszacowania potencjalnej wielkości dla największego udziałowca przez udziałowca dalszego w kolejności może być bardzo duża. Zaproponowana metoda umożliwia znaczną redukcję tego ryzyka. Pozwala to też na zaostrzenie reguły *(n,k)* dominacji w razie potrzeby.

Obliczając ryzyko należy pamiętać także i o tym, że użytkownik może dysponować alternatywnymi źródłami danych, które – mimo opracowania przez statystykę publiczną tablicy według powyższych reguł – mogą pozwolić na identyfikację jednostki. Trzeba przy tym uwzględnić następujące kierunki ryzyka (zob. np. Hundepool i in. [-@hundepool2012statistical], Młodak i in. [-@mlodak2023poufnosc]):

* ujawnienie atrybutu jednostki – dotyczy sytuacji, kiedy w pierwszej kolejności dochodzi do identyfikacji jednostki, czyli poprzez niewielkie wartości komórek w tablicy. Wartość komórki lub wartość brzegowa ma np. wartość 1 i na tej podstawie dochodzi do identyfikacji jednostki. Następnie poprzez inne publikacje oparte o to samo źródło danych uzyskane zostają dodatkowe informacje o zidentyfikowanej jednostce; na przykład: niech z jednej z tablic wynika, że w gminie wiejskiej mieszka tylko jedna uczennica danej szkoły, wówczas na podstawie tej informacji i danych z innej tablicy (np. struktury uczniów według miejsca zamieszkania, wieku, promocji do następnej klasy) można odczytać wiele wrażliwych informacji o niej.
* ujawnienie atrybutu grupy – uzyskanie dodatkowych informacji o zidentyfikowanej grupie (to znaczy o grupie jednostek zdefiniowanej przez wspólną wartość określonej cechy (np. osoby w wieku 20-25 lat czy osoby mieszkające w określonej gminie) lub o tym, że zidentyfikowana grupa jakiegoś atrybutu nie posiada. Hundepool i in. [-@hundepool2012statistical] wskazują, że jest to bardzo często zaniedbywany element ochrony informacji objętych tajemnicą statystyczną. Niebezpieczeństwo ujawnienia danych wrażliwych występuje w przypadkach, gdy wszystkie lub prawie wszystkie jednostki należą do jednej kategorii. Dla przykładu: jeżeli w pewnym obszarze jest 10 mężczyzn w wieku 65–70 lat, a wszyscy oni pracują w niepełnym wymiarze czasu pracy, to *de facto* następuje ujawnienie informacji o formie zatrudnienia wszystkich tych mężczyzn.
* ujawnienie przez łączenie – poprzez relację łączącą dwie lub więcej tablic możliwe jest uzyskanie dodatkowej informacji. Analiza wspólnych zmiennych występujących w różnych tablicach stwarza możliwość uzyskania informacji identyfikującej jednostkę. Można tutaj wskazać trzy kierunki takiego łączenia:
  * różnicowanie geograficzne: np. jeśli w jednym roku w pewnej gminie mieszkało 34 Ukraińców, a w następnym roku nastąpiło administracyjne powiększenie obszaru gminy, w wyniku czego opublikowano, że takich Ukraińców jest 35 (przy czym w ciągu tego roku żaden nie wyjechał ani nie zmarł, ani nie przybył z zagranicy), to wiadomo, że na dołączonym obszarze mieszka jeden Ukrainiec,
  * łączenie poprzez wspólne zmienne: wykorzystanie tych samych zmiennych występujących w dwóch lub więcej różnych tablicach do połączenia tych tablic i uzyskania informacji wrażliwych; dla przykładu, mamy 3 publikowane tablice (przykład inspirowany egzemplifikacją z książki Hundepool i in. [-@hundepool2012statistical])
  
  ![](fig/r7.1.png)
  
  ![](fig/r7.2.png)

  ![](fig/r7.3b.png)

  ![](fig/r7.4.png)

  * tablice subpopulacyjne: odnoszą się do wcześniej zdefiniowanej subpopulacji; na przykład liczba zachorowań na nowotwór gruczołu krokowego (prostaty) dotyczyć będzie z definicji tylko mężczyzn. Na przykład, jeżeli w jednej tablicy prezentowane są zachorowania na ten nowotwór w obszarze A w grupach wiekowych: do 40 lat, 40-45 lat, 45-50 lat, 50-55 lat, 55-60 lat, 60-65 lat, 65-70 lat, 70-75 lat, 75-80 lat, 80-85 lat oraz 85 i więcej lat, a w drugiej (być może zamieszczonej w innym źródle) w tym obszarze A w grupach: do 50 lat, 50-67 lat, 68-75 lat, 75-85 lat, 85-90 lat oraz 90 i więcej lat, to np. jeśli z pierwszej tablicy wynika, że na nowotwór prostaty chorowało 7 mężczyzn w wieku 50-55 lat, 9 – w wieku 55-60 lat, 10 – w wieku 60-65 lat i 8 w wieku 65-70 lat, a z drugiej tablicy wnioskujemy, że zachorowało 33 mężczyzn w wieku 50-67 lat, to można stąd wywnioskować, że na nowotwór prostaty cierpiał tylko jeden mężczyzna w wieku 67-70 lat.

W przypadku badań reprezentacyjnych indywidualne ryzyko ujawnienia i-tej jednostki w próbie definiowane jest jako prawdopodobieństwo wtórnej identyfikacji według najgorszego przypadku scenariusza (zgodnie z założeniami powyżej). Najgorszy scenariusz ujawnienia zaś to taki, w ramach którego łatwość dostępu do określonych źródeł danych (w statystyce publicznej lub poza nią) i posługiwanie się nimi stwarzają największe zagrożenie ujawnieniem informacji wrażliwych poprzez łączenie odpowiednich danych. Na przykład w przypadku Badania Aktywności Ekonomicznej Ludności stosunkowo najłatwiejszy dla potencjalnego użytkownika byłby dostęp do alternatywnych źródeł danych (opartych np. na rejestrze bezrobotnych, wyborców czy danych ze spisu powszechnego), a zatem połączenie takich informacji celem identyfikacji jednostki jest najbardziej prawdopodobne (nawet jeśli poszczególne zbiory – jak spisowy – byłyby chronione przez odpowiednie ingerencje w dane oryginalne). Inne źródła administracyjne, które mogłyby tutaj wchodzić w grę (np. rejestr podatkowy – POLTAX czy ewidencja ludności – PESEL) są zazwyczaj trudniej dostępne. 

Ryzyko indywidualne wyznacza się tu według następującej reguły:

$$r_i=P(i \text{ jest poprawnie dopasowane do } i^*│s, \mathscr{P}, \text{ najgorszy możliwy scenariusz }),$$

gdzie $\mathscr{P}$ oznacza się populację, a $s$ – próbę z niej pobraną, która wraz z wagami wynikającymi ze schematu losowania (odpowiadającymi odwrotnościom prawdopodobieństw wylosowania jednostek do próby) ma zostać udostępniona. Z kolei $i$ to jednostka z próby, zaś $i^*$ – jednostka w populacji generalnej.

Ryzyko wtórnej identyfikacji (czyli ryzyko identyfikacji jednostki na podstawie wartości quasi identyfikatorów) określa się na poziomie obserwacji dla pojedynczej jednostki (obserwowanej w danych jednostkowych jako pojedynczy rekord). Oznaczmy przez $F_j$ liczbę jednostek w populacji, ale przez $f_j$ – liczbę jednostek w próbie przynależących do $j$–tej komórki (gdzie $j$ jest liczbą naturalną). Miara globalnego ryzyka ujawnienia, na poziomie zbioru danych, oznaczana jako $\tau_1$, to suma miar indywidualnego ryzyka ujawnienia dla wszystkich $j$, dla których liczba jednostek w populacji jest dodatnia, czyli (zob. np. Hundepool i in. [-@hundepool2012statistical]):

$$\tau_1=\sum_{j:F_j>0}\frac{1}{F_j}$$ {#eq-tau1}

Miarą ryzyka indywidualnego jest tutaj odwrotność liczby jednostek należących do danej komórki w populacji. W tym znaczeniu jest to prawdopodobieństwo zdarzenia polegającego na tym, że dana liczebność komórki w populacji pozwala na identyfikację (co najmniej jednej) jednostki. Jeżeli badanie ma charakter pełny, wówczas $f_j=F_j$ dla każdego $j$. Gdy zaś badanie jest reprezentacyjne, wtedy $F_j$ szacuje się np. jako sumę wynikających z zastosowanego w badaniu schematu losowania wag dla jednostek wylosowanych do próby i należących do $j$-tej komórki tablicy. Takie określenie ryzyka wynika z faktu, że w jego ocenie należy wziąć pod uwagę możliwość identyfikacji jednostki zarówno na podstawie danych jednostkowych, jak też na podstawie danych dla populacji.  

Ponieważ wartości unikalne w populacji generalnej (czyli takie, dla których $F_j=1$) są przeważającym czynnikiem ryzyka ujawnienia, uwagę należy skupić na wartościach unikalnych w próbie (tzn. tych, dla których $f_j=1$). Stąd często ogranicza się wyznaczanie miary ryzyka do komórek, do których w próbie należą jednostki unikalne (a więc takie, że odpowiadające im kombinacje wartości zmiennych definiujących komórkę, występują w tej komórce tylko jedno– lub dwukrotnie, co jest także zgodne z zapisami Ustawy).

Biorąc powyższe pod uwagę, oczekiwaną wartość trafnych odgadnięć, w przypadku gdy każda jednostka unikalna z próby jest dopasowana do jednostki wybranej losowo z tej samej komórki dla populacji (bez względu na zastosowany schemat losowania), zapisuje się następująco:

$$\tau_2=\sum_{j}I(f_j=1)\frac{1}{F_j},$$ {#eq-tau2}

lub (z uwagi na polskie uregulowania):

$$\tau^{*}_2=\sum_{j}\left(I(f_j=1)\frac{1}{F_j}+I(f_j=2)\frac{1}{F_j}\right),$$ {#eq-tau3}

gdzie $I(∙)$ pojawiające się w powyższym wzorze to funkcja indykatorowa, przyjmująca wartość jeden, gdy spełnione jest dane kryterium lub zero w przeciwnym przypadku. Innymi słowy, jest to suma @eq-tau1 obcięta tylko do komórek, których liczebność w próbie jest równa 1 (@eq-tau2) albo nie większa od 2 (@eq-tau3). Zauważmy, że nie jest tu już potrzebne założenie, że $F_j>0$, gdyż warunek ten wynika wprost z niezerowej liczebności w próbie.    
Alternatywną definicją rozpatrywanej miary ryzyka jest liczba unikalnych jednostek w próbie $(f_j=1)$, które są jednocześnie unikalne w przypadku populacji generalnej $(F_j=1)$:

$$\tau_2=\sum_{j}I(f_j=1,F_j=1)$$ {#eq-tau4}


Powyższe miary można przedstawić w postaci wskaźników. Wtedy wielkości $\tau_1$, $\tau_2^{*}$ i $\tau_3$ należy podzielić przez rozmiar próby lub liczbę unikalnych jednostek w próbie. Mierniki takie wyznaczane są np. w programie $\tau$–Argus czy pakiecie sdcTable środowiska R i okazują się wygodne w stosowaniu. **Można zatem rekomendować stosowanie tych miar w praktyce statystycznej.** 

## Dane jednostkowe

W przypadku danych jednostkowych nieidentyfikowalnych metody oceny ryzyka ujawnienia ściśle wiążą się z rodzajem skali pomiarowej, na jakiej mierzone są dane z zakresu poszczególnych zmiennych. Inne miary w tym zakresie przeznaczone są bowiem dla zmiennych kategorialnych (tzn. wyrażonych na skali nominalnej lub porządkowej), a inne dla zmiennych ciągłych (czyli wyrażonych na skalach: różnicowej lub ilorazowej).

W przypadku danych kategorialnych podstawą konstrukcji miar ryzyka są częstości występowania poszczególnych możliwych kombinacji wartości takich cech. **Określa się zatem minimalną częstość występowania danej kombinacji stanowiącą warunek konieczny ochrony przed identyfikacją jednostki (zasada** ***k*** **– anonimowości). W polskiej rzeczywistości prawnej będzie to 3 (lub więcej, w razie potrzeby)**. Z tym wiąże się też sposób traktowania braków danych. Templ [-@templ2017statistical] podaje, że występuje pięć sposobów:

* sposób klasyczny: braki danych zwiększają częstości wszystkich kombinacji, których mogą dotyczyć (brak danych traktuje się jak każdą możliwą wartość),
* sposób konserwatywny: brakujące wartości nie zwiększają częstości wszystkich kombinacji, których mogą dotyczyć, ale jedynie tych, w których braki te występują (brak danych traktuje się jako odrębną kategorię),
* sposób kategorialny: brakujące wartości danych zwiększają częstości wszystkich kombinacji, których mogą dotyczyć o ustaloną stałą $c$,
* sposób skrajnie konserwatywny: brakujące wartości nie zwiększają częstości żadnych kombinacji (braki danych nie są traktowane tożsamo), 
* sposób własnokategorialny: brakujące wartości nie zwiększają częstości żadnych kombinacji, jednak braki te traktowane są jako odrębna kategoria.

Tablica 4 pokazuje przykład praktycznej realizacji tych sposobów dla trzech zmiennych kategorialnych i 10 rekordów: 

![](fig/r7.4.png){fig-align=center}

![](fig/r7.4b.png){fig-align=center}

Wydaje się, że zastosowanie sposobu klasycznego może prowadzić do niedoszacowania faktycznego ryzyka ujawnienia (np. kombinacje z brakami danych zwiększą częstość unikatowych kombinacji pełnych), co istotnie zwiększa zagrożenie identyfikacji jednostki. Z kolei podejście skrajnie konserwatywne czy opcja własnokategorialna mogą powodować przeszacowywanie ryzyka (nadmiar kategorii unikatowych w kombinacjach z brakami danych). Dlatego też można polecić do stosowania przede wszystkim sposób konserwatywny, względnie kategorialny (gdy na podstawie wcześniejszych doświadczeń i płynącej z niej oceny poziomów i kierunków typowych braków danych) można określić typową lub przeciętną wartość stałej $c$).

Z uwagi na to, że użytkownik może dysponować alternatywnymi źródłami danych z określonego zakresu, stosując kontrolę ujawniania danych należy wziąć pod uwagę potencjalne zbiory, do jakich teoretycznie może mieć dostęp użytkownik – np. jeśli pracuje on w instytucji zajmującej się prowadzeniem ewidencji ludności lub analizami demograficznymi, to może mieć on dostęp do rejestru PESEL i zawartych w nim informacji. Wtedy ryzyko ujawnienia trzeba oceniać w powiązaniu z tym źródłem. Innymi słowy, poziom ryzyka wynikającego z czystej konstrukcji tablicy należy powiększyć o ryzyko wynikające z unikatowości określonych wielkości w potencjalnie używanym przez użytkownika zbiorze alternatywnym. Warto podkreślić, że takie rozpoznanie ma na celu przede wszystkim uwzględnienie rzeczonej możliwości w (wewnętrznej) ocenie ryzyka ujawnienia a nie określenie zakresu danych udostępnianych użytkownikowi. Nie można jednak wykluczyć, że wywrze to wpływ na ostateczne decyzje w zakresie dostępu do danych, które jednak zawsze muszą mieć indywidualny charakter.  

**Wobec powyższego, do określenia faktycznego poziomu ryzyka ujawnienia stosuje się często mocniejszą regułę** ***l*** **– różnorodności**. Wymaga to wyodrębnienia zestawu zmiennych kluczowych (które w największym stopniu wpływają na ryzyko identyfikacji jednostki) i zmiennych wrażliwych niebędących kluczowymi (te zazwyczaj są poddawane post–randomizacji). W stosunku do zmiennych kluczowych stosuje się wówczas klasyczną regułę *k*–anonimowości, a następnie bada czy każda kombinacja tych zmiennych zawiera przynajmniej *l* wartości zmiennej wrażliwej o odpowiednio wysokiej częstości (wynikającej z obowiązujących regulacji prawnych – np. co najmniej 3 w Polsce – lub z doświadczenia). To dodatkowe sprawdzenie jest konieczne, jako że – jak już wcześniej zauważono – samo stosowanie zasady *k*-anonimowości może prowadzić do tworzenia grup niezróżnicowanych pod kątem wrażliwych atrybutów oraz z powodu niedostateczności ochrony samą zasadą *k*-anonimowości przed identyfikacją jednostki prostymi metodami (np. poprzez wiązanie tablic). Więcej na ten temat piszą np. Machanavajjhala i in. [-@machanavajjhala2007diversity]. Stosując zatem regułę *l*–różnorodności można wyznaczyć opisowe statystyki zróżnicowania wyrażonego liczbą różnych wartości zmiennej wrażliwej obserwowanych dla poszczególnych kombinacji wartości zmiennych kluczowych. 

**W przypadku badania reprezentacyjnego, w ocenie ryzyka należy uwzględnić również wagi wynikające ze schematu losowania, pozwalające na oszacowanie częstości występowania poszczególnych kombinacji w populacji.** Programy komputerowe, takie jak np. pakiet sdcMicro środowiska R, podają wtedy częstości występowania poszczególnych kombinacji zarówno w próbie, jak i w populacji. Jeżeli niektóre zmienne pozyskiwane są w sposób pełny (np. ze źródeł administracyjnych), to można też porównać populacyjne częstości kombinacji wartości tych zmiennych z odpowiednimi oszacowaniami.
Oczywiście nawet niebezpieczne kombinacje wartości (czyli o częstości niższej niż arbitralnie ustalony próg) nie muszą stwarzać jednakowego ryzyka identyfikacji jednostki. Różnice te wynikają m.in. z faktu, że podzbiory wartości należących do kombinacji niebezpiecznej same są kombinacjami (choć mniejszego rozmiaru), mogą zatem być kombinacjami niebezpiecznymi lub nie. Dlatego też stworzono pojęcie minimalnej kombinacji unikatowej (ang. *Minimal Sample Unique, MSU*), czyli kombinacji niebezpiecznej, której żaden podzbiór właściwy nie stanowi kombinacji niebezpiecznej. Na przykład, taką kombinacją może być kombinacja (1,2,5), której częstość wynosi 1, podczas gdy częstości podkombinacji są następujące (1,2) – 5, (2,5) – 4, (1,5) – 7, {1} – 10 , {2} – 8 i {5} – 7. **Ocenę ryzyka z uwzględnieniem MSU umożliwia specjalny algorytm wykrywania kombinacji unikalnych (ang.**  ***Special Uniques Detection Algorithm, SUDA)***, oparty na założeniach, że im mniejsza jest liczba zmiennych należących do MSU lub im większa jest liczba MSU zawartych w rozpatrywanej kombinacji, tym ryzyko ujawnienia jest większe. 

W efekcie otrzymujemy wykaz zmiennych wraz z ich **ważnościami**. Ważność zmiennej określa się zaś jako wkład do ryzyka ujawnienia, a konkretnie – to jak często kategoria danej zmiennej stanowi wkład do wyniku dla MSU (tzw. współczynnik *score for MSU*). Innymi słowy, bazuje to na liczbie minimalnych kombinacji unikatowych, które zawierają jakąkolwiek kategorię danej zmiennej. Szerzej pisze o tym np. Templ [-@templ2017statistical]. 

Algorytm SUDA **dostępny** jest m.in. w pakiecie sdcMicro środowiska R. Szczegóły na temat algorytmu SUDA podają np. Elliott, Manning i Ford [-@elliot2002computational]. **Algorytm ten można polecić do stosowania.**

Istotną rolę w efektywnym przebiegu procesu kontroli ujawniania danych odgrywa ocena ryzyka indywidualnego dla każdego rekordu. Ryzyko to odnosi się do częstości występowania kombinacji wartości zmiennych kluczowych z danego rekordu w rozpatrywanym zbiorze danych. Stanowi jedną z fundamentalnych miar ryzyka i jest użyteczne zazwyczaj w każdym przypadku, ale szczególnie – dla danych z badania reprezentacyjnego. Wartości miary należą do przedziału [0,1]. Im większa wartość miary, tym większe ryzyko. Algorytm prowadzący do jego wyznaczenia bazuje bowiem na modelu nadpopulacyjnym, w którym częstości w populacji szacowane są według przyjętego arbitralnie rozkładu teoretycznego. Najczęściej stosowany jest tutaj model Benedettiego–Franconi, w którym zakłada się, że częstości z próby mają rozkład Poissona z parametrem $\lambda=N\pi_k$, gdzie $N$ to liczba jednostek w populacji, $\pi_k$ – prawdopodobieństwo wylosowania do próby jednostki z daną kombinacją wartości zmiennych kategorialnych. Częstość kombinacji owych wartości w populacji warunkowana odpowiednią częstością w próbie posiada zaś w tym przypadku ujemny rozkład dwumianowy $-b(p_j,f_j)$ z prawdopodobieństwem sukcesu będącym szacunkiem ilorazu częstości w próbie i szacowanej częstości w populacji $(p_j=f_j/F ̂_j)$ oraz z liczbą sukcesów równą rzeczonej częstości w próbie $(f_j)$. Szczegóły na ten temat podają np. Templ [-@templ2017statistical] czy Franconi i Polettini [-@franconi2004individual]. Ryzyko indywidualne jest w ten sposób wyznaczane np. w programie $\mu$–Argus i pakiecie sdcMicro środowiska R. Warto tę metodę polecić do praktycznego wykorzystania. W przypadku gdy w danych występuje określona hierarchia jednostek i ich agregatów, ryzyko dla takiego agregatu (np. gospodarstwa domowego gdy jednostkami są osoby) wyznaczane jest na podstawie wzoru: $r_A=1-\Pi_{i:i\in A}(1-r_i)$, gdzie $A$ to dany agregat, zaś $r_i$ – ryzyko indywidualne dla $i$–tej jednostki. Oznacza to, że opiera się ono na iloczynie prawdopodobieństw, że kombinacje wchodzące w skład danego agregatu są bezpieczne z punktu widzenia ochrony danych wrażliwych. Zakres wartości i interpretacja miary ryzyka hierarchicznego jest analogiczna jak w przypadku ryzyka indywidualnego. **Ocena ryzyka indywidualnego (w tym hierarchicznego, jeśli hierarchia występuje) powinna stać się fundamentem działań w zakresie SDC.** Ryzyko takie należy oceniać w każdych badaniach, w których występują zmienne kategorialne, a jeżeli mamy tam i hierarchie (np. jednostek przestrzennych, podmiotów według PKD, osoba-gospodarstwo domowe, itp.), to również ryzyko hierarchiczne.   

**Nie mniej ważna jest także ocena ryzyka globalnego.** Otrzymujemy tutaj jedną kompleksową miarę ryzyka dla całego zbioru. Możliwe są tutaj trzy alternatywne podejścia:

* oczekiwana liczba reidentyfikacji (suma ryzyk indywidualnych dla poszczególnych rekordów),
* modele logarytmiczno-liniowe (bazujące na szacowaniu liczby unikatowych kombinacji w próbie, które są jednocześnie unikatowymi w populacji),
* podejście benchmarkowe (oparte na liczbie obserwacji o indywidualnym poziomie ryzyka wyższym niż arbitralnie ustalony próg; próg ten ustala osoba dokonująca ochrony danych w oparciu o własności danego badania i zgromadzonych w nim informacji wynikowych, w tym rozmiaru i struktury danych, schematu doboru próby oraz cech jednostek; na przykład jeśli próba będzie mała a badanie obejmuje znaczną liczbę zmiennych kategorialnych o różnorodnych kategoriach, to rzeczony próg powinien być przyjęty na znacznie niższym poziomie aniżeli w przypadku badania bazującego na  próbie dużego rozmiaru ze stosunkowo niezbyt znaczną liczbą zmiennych kategorialnych o małej liczbie kategorii).

Szczegółowo metody te opisuje np. Templ [-@templ2017statistical]. W pakiecie sdcMicro stosowane jest pierwsze podejście, czyli oczekiwana liczba reidentyfikacji. Podobnie rzecz się ma w przypadku programu $\mu$–Argus, przy czym tam suma ryzyk indywidualnych dzielona jest przez liczbę rekordów.

Powyższe uwagi i sugestie dotyczą zmiennych kategorialnych. Jednak pozostaje jeszcze kwestia wyznaczania poziomu ryzyka ujawnienia dla zmiennych ciągłych, które posiadają swoją własną specyfikę, związaną zwłaszcza z teoretyczną możliwością przyjmowania nieskończonej liczby różnych wartości, na których w dodatku można wykonywać określone działania arytmetyczne. Tym samym nie mogą być tutaj zastosowane miary ryzyka oparte na częstościach (o ile zmienne te nie będą udostępniane w postaci skategoryzowanej, np. poprzez częstości jednostek, dla których wartości takiej zmiennej należą do określonych przedziałów). Wskaźniki dla tego typu zmiennych muszą być zatem innego rodzaju. W tym względzie pakiet sdcMicro środowiska R jako miarę ryzyka wykorzystuje odsetek obserwacji należących do przedziału, którego środkiem jest zakłócona w procesie SDC wartość, podczas gdy jego górna granica zdefiniowana została jako najgorszy możliwy scenariusz gdy użytkownik jest pewien, że każdy rekord ze zbioru oryginalnego najbliższy odpowiedniemu rekordowi w drugim (czyli zaburzonym) zbiorze jest faktycznie tożsamy z tym rekordem. Szerzej na ten temat piszą m.in. Mateo–Sanz i in. [-@mateo2004outlier]. Program $\mu$-Argus nie posiada oddzielnych miar ryzyka ujawnienia dla zmiennych ciągłych, ale ryzyko to można oszacować np. kategoryzując zmienne rozpatrywanego typu.

Jak wynika z powyższych informacji, problemem jest brak uniwersalnej miary ryzyka dla całego zbioru, zawierającego zarówno zmienne kategorialne, jak i zmienne ciągłe. Jednak można by rozważyć możliwość skonstruowania takiej miary w oparciu o metodę wiązania rekordów opartej na odległości (zob. np. Pagliuca i Seri [-@pagliuca1999some]). Dla każdego rekordu w zbiorze po zastosowaniu SDC obliczamy odległość od każdego rekordu ze zbioru oryginalnego i na tej podstawie wskazujemy najbliższego i drugiego co do bliskości sąsiadów. Jeśli rekord ze zbioru po SDC i rekord jemu najbliższy ze zbioru oryginalnego odnoszą się do tej samej jednostki, to uznajemy je za powiązane bezpośrednio. Podobnie jeśli do tej samej jednostki odnoszą się: rekord ze zbioru zniekształconego i drugi najbliższy mu rekord ze zbioru oryginalnego, wtedy mówimy, że występuje między nimi powiązanie drugiego stopnia. Finalną miarę ryzyka stanowi odsetek rekordów ze zbioru po SDC, dla których występuje powiązanie bezpośrednie lub powiązanie drugiego stopnia. 

Inną propozycję zawiera praca Młodaka, Pietrzaka i Józefowskiego [-@mlodak2022trade]. Wychodzi ona z założenia, że – ogólnie rzecz biorąc – można wyróżnić dwa rodzaje ryzyka ujawnienia informacji wrażliwych w zbiorze danych powstałym w efekcie zastosowania procesu SDC:

* ryzyko wewnętrzne – gdy występuje groźba ujawnienia informacji wrażliwych tylko w oparciu o zmodyfikowane dane (warto zauważyć, że miary ryzyka wewnętrznego mogą też być w sposób oczywisty użyte do oceny ryzyka ujawnienia przed zastosowaniem SDC),
* ryzyko zewnętrzne – jeżeli występuje zagrożenie identyfikacji jednostki poprzez powiązanie danych udostępnionych po przeprowadzeniu procesu SDC z informacjami pochodzącymi z innych źródeł, do których użytkownik potencjalnie może mieć dostęp.

Ryzyko wewnętrzne wynika – jak już wcześniej wskazywaliśmy – z faktu występowania jednoznacznych kombinacji wartości (dokładnie dla zmiennych kategorialnych oraz – jeśli dotyczy – w obrębie pewnego przedziału precyzji dla zmiennych ciągłych). Odnosi się ono do zagrożenia identyfikacją jednostki tylko przy użyciu informacji zawartych w udostępnianym zbiorze. W tym przypadku przyjmuje się, że użytkownik może opierać się jedynie na informacji, którą statystyka publiczna mu udostępnia.

Z kolei ryzyko zewnętrzne zależy od możliwości powiązania rekordów zawartych w zbiorze danych statystycznych (poddanemu procesowi SDC) z odpowiednimi rekordami z innych źródeł danych dostępnych użytkownikowi. Odnosi się ono zatem do sytuacji, w której użytkownik może sięgnąć do alternatywnych źródeł danych w celu identyfikacji jednostki poprzez połączenie odpowiednich rekordów z wszystkich tych zasobów.

Autorzy wskazanej wyżej pracy zaproponowali połączenie obu typów ryzyka ujawnienia oraz sposób kompleksowego pomiaru ryzyka zewnętrznego uwzględniającego zmienne, których dane wyrażone są na różnych skalach pomiarowych. Idea tej metody wywodzi się z koncepcji odległości Gowera.

Podsumowując, konieczne jest wyznaczenie ryzyka indywidualnego i globalnego (a w razie potrzeby także i hierarchicznego) dla zmiennych kategorialnych, ryzyka dla zmiennych ciągłych, a w miarę możliwości także określenie ważności poszczególnych zmiennych z punktu widzenia ich wpływu na wielkość ryzyka.

Na zakończenie należy zauważyć, że ocena ryzyka identyfikacji jednostki może być przeprowadzona w następujących fazach schematu GSBPM (MPPS):

* Projektowanie
* Przetwarzanie i analiza
* Ocena.

Na etapie projektowania badania ryzyko ujawnienia może być szacowane w zasadzie wyłącznie na podstawie jego zasad metodologicznych bądź doświadczeń wyniesionych z wcześniejszych jego edycji (jeśli takie były). W pozostałych dwóch fazach oceny ryzyka dokonuje się już w oparciu o zgromadzone informacje, przed i po ich poddaniu kontroli ujawniania.
Zgodnie z Polityką postępowania z danymi statystycznymi zaleca się, aby wybór metody bądź metod kontroli ujawniania danych był poprzedzony oceną ryzyka ujawnienia informacji poufnych z jednoczesną oceną ponoszonej straty informacji.


:::

